import java.util.ArrayList;
import java.util.Scanner;

public class JPointTask2 {

    static final String[] allDigits = {
            " _     _  _     _  _  _  _  _ ",
            "| |  | _| _||_||_ |_   ||_||_|",
            "|_|  ||_  _|  | _||_|  ||_| _|",
    };
    static final int digitHeight = 3;
    static final int digitWidth = 3;

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        var lines = new ArrayList<String>();
        while (scanner.hasNext()) {
            lines.add(scanner.nextLine());
        }
        System.out.print(parseDigits(lines));
    }

    private static int parseDigits(ArrayList<String> lines) {
        if (lines.size() != digitHeight) {
            throw new IllegalArgumentException("Invalid lines count");
        }

        int lineWidth = lines.get(0).length();
        if (lineWidth % digitWidth != 0) {
            throw new IllegalArgumentException("Line #1 has invalid length");
        }
        for (int i = 1; i < lines.size(); ++i) {
            if (lines.get(i).length() != lineWidth) {
                throw new IllegalArgumentException("Line #" + i + "has invalid length");
            }
        }

        int digitCount = lineWidth / digitWidth;
        int result = 0;
        for (int pos = 0; pos < digitCount; ++pos) {
            int digit = parseDigitAtPos(lines, pos);
            if (digit == -1) {
                throw new IllegalArgumentException("Invalid digit at position " + pos);
            }
            result = digit + result * 10;
        }
        return result;
    }

    static int parseDigitAtPos(ArrayList<String> lines, int pos) {
        for (int digit = 0; digit <= 9; ++digit) {
            boolean match = true;
            for (int row = 0; row < digitHeight; ++row) {
                String segment1 = lines.get(row).substring(pos * digitWidth, (pos + 1) * digitWidth);
                String segment2 = allDigits[row].substring(digit * digitWidth, (digit + 1) * digitWidth);
                if (!segment1.equals(segment2)) {
                    match = false;
                    break;
                }
            }
            if (match) {
                return digit;
            }
        }
        return -1;
    }
}
