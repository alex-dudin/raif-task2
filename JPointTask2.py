﻿import unittest
from typing import List, Optional


DIGITS = [
    ' _     _  _     _  _  _  _  _ ',
    '| |  | _| _||_||_ |_   ||_||_|',
    '|_|  ||_  _|  | _||_|  ||_| _|',
    ]

DIGIT_WIDTH = 3
DIGIT_HEIGHT = 3


def parse_digit_at_pos(lines: List[str], pos: int) -> Optional[int]:
    '''Parse seven-segment digit at the specified position.'''
    
    def is_same(lines1, pos1, lines2, pos2):
        for line1, line2 in zip(lines1, lines2):
            segment1 = line1[pos1 * DIGIT_WIDTH : (pos1 + 1) * DIGIT_WIDTH]
            segment2 = line2[pos2 * DIGIT_WIDTH : (pos2 + 1) * DIGIT_WIDTH]
            if segment1 != segment2:
                return False
        return True

    for digit in range(0, 10):
        if is_same(lines, pos, DIGITS, digit):
            return digit

    return None


def parse_digits(lines: List[str]) -> int:
    '''Parse seven-segment digits.'''
    if len(lines) != DIGIT_HEIGHT:
       raise ValueError('Invalid lines count')
    
    line_width = len(lines[0])
    if line_width % DIGIT_WIDTH != 0:
       raise ValueError('Line #1 has invalid length')

    for i in range(1, len(lines)):
        if len(lines[i]) != line_width:
           raise ValueError(f'Line #{i} has invalid length')

    digit_count = line_width // DIGIT_WIDTH
    result = 0
    for pos in range(0, digit_count):
        digit = parse_digit_at_pos(lines, pos)
        if digit is None:
            raise ValueError(f'Invalid digit at position {pos}')
        result = digit + result * 10

    return result


class TestParseDigits(unittest.TestCase):

    def test_example1(self):
        lines = [
            '    _  _     _  _  _  _  _ ',
            '  | _| _||_||_ |_   ||_||_|',
            '  ||_  _|  | _||_|  ||_| _|',
            ]
        self.assertEqual(parse_digits(lines), 123456789)

    def test_example2(self):
        lines = [
            ' _  _  _  _  _  _  _  _  _ ',
            '| | _| _|| ||_ |_   ||_||_|',
            '|_||_  _||_| _||_|  ||_| _|',
            ]
        self.assertEqual(parse_digits(lines), 23056789)

    def test_example3(self):
        lines = [
            ' _  _  _  _  _  _  _  _  _ ',
            '|_| _| _||_||_ |_ |_||_||_|',
            '|_||_  _||_| _||_| _||_| _|',
            ]
        self.assertEqual(parse_digits(lines), 823856989)


if __name__ == '__main__':
    import sys
    print(parse_digits(sys.stdin.read().splitlines()))
    